var restful = require('node-restful');
var mongoose = restful.mongoose;

var schema = new mongoose.Schema({
  title: String,
  description: String,
});

restful.model('Talk', schema);
