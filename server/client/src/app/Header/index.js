import React from 'react';
import Container from '../layout/Container';
import Section from '../layout/Section';

const Header = () => (
  <Section color="#f8bbd0" className="h3">
    <Container>I am Header!</Container>
  </Section>
);

export default Header;
