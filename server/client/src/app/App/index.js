import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Container from '../layout/Container';

import Header from '../Header';
import TalkList from '../../talks/TalkList';
import TalkView from '../../talks/TalkView';
import TalkCreate from '../../talks/TalkCreate';
import TalkEdit from '../../talks/TalkEdit';

const App = () => {
  return (
    <div>
      <div>
        <Header />
        <BrowserRouter>
          <Container>
            <Route exact path="/" component={TalkList} />
            <Route path="/view" component={TalkView} />
            <Route path="/create" component={TalkCreate} />
            <Route path="/edit" component={TalkEdit} />
          </Container>
        </BrowserRouter>
      </div>
    </div>
  );
};

export default App;
