import styled from 'styled-components';

const Container = styled.div.attrs({
  className: 'w-100 w-80-ns',
})`
  margin: auto;
`;

export default Container;
