import styled from 'styled-components';

const Section = styled.div.attrs({
  className: 'w-100',
})`
  background-color: ${(props) => props.color};
`;

export default Section;
