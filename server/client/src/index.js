import React from 'react';
import ReactDOM from 'react-dom';
import 'tachyons/css/tachyons.min.css';
import App from './app/App';

ReactDOM.render(<App />, document.getElementById('root'));
