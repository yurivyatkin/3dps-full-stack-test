var restful = require('node-restful');
var Talk = restful.mongoose.model('Talk');
var requreLogin = require('../middlewares/requireLogin');

module.exports = (app) => {
  Talk.methods(['get', 'post']);
  Talk.before('post', requreLogin);
  Talk.register(app, '/api/talks');
};
